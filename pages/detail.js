import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import ButtonAdd from "../component/buttonAdd";
import ButtonLast from "../component/buttonLast";
import Header from "../component/header";
import Rating from "../component/rating";
import StatusStok from "../component/statsustok";

export default function DetailItem() {
  const [post, setPost] = useState([]);
  const [choice, setChoice] = useState([]);
  const router = useRouter();

  useEffect(() => {
    axios
      .get(
        "https://recruitment.dev.rollingglory.com/api/v2/gifts?page[number]=1&page[size]=20"
      )
      .then((response) => {
        setPost(response.data);
      });
  }, []);

  useEffect(() => {
    if (router && post) {
      const id = router.query.itemId;
      const data = post?.data?.filter((row) => row.id === id);
      setChoice(data);
    }
  }, [router, post]);
  return (
    <div className="container">
      <Header />
      {choice === undefined ? (
        <>
          <p onClick={() => router.push("/")}>List Item</p>
          <h1>No Data</h1>
        </>
      ) : (
        choice.map((data) => (
          <>
            <p>
              <a
                style={{ cursor: "pointer", color: "blue" }}
                onClick={() => router.push("/")}
              >
                List Item
              </a>{" "}
              {'>'} {data.attributes?.name}
            </p>
              <div className="row mb-4 mt-4 ms-5" style={{  fontFamily: "Times New Roman" }}>
                <div className="col">
                  <div className="w-100">
                    <div className="d-flex justify-content-between">
                      <div></div>
                    </div>
                    <img
                      src={data.attributes.images[0]}
                      width="300px"
                      height="300px"
                    />
                  </div>
                </div>
                <div className="col">
                <p style={{  fontFamily: "Times New Roman", fontSize: '50px', fontWeight: 'bold' }}>{data.attributes.name}</p>
                    <Rating key={data} total={data.attributes.rating || 1} view={data.attributes.numOfReviews}/>
                    <div className='fs-6 text-success d-flex col' style={{  fontFamily: "Times New Roman" }}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-x-circle-fill mt-1" viewBox="0 0 16 16">
                      <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z"/>
                  </svg>
                    <p className='text-success ms-1' style={{  fontSize: '20px', fontWeight: 'bold' }}>{parseInt(data.attributes.points).toLocaleString('ID-id')} poins</p>
                    <p className="ms-2 mt-1"><StatusStok stok={data.attributes.stock} /></p>
                  </div><p>{data.attributes.info}</p>
                  <ButtonAdd />
                  <ButtonLast like={data.attributes.isWishlist} />
                </div>
              </div>
              
          </>
        ))
      )}
    <p className="mt-5 text-success">Info</p>
    </div>
  );
}
