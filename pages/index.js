import axios from 'axios'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import CardSmall from '../component/cardsmall'
import Header from '../component/header'
import Like from '../component/like'
import Rating from '../component/rating'
import StatusStok from '../component/statsustok'

export default function Home() {
  const [post, setPost] = useState([]);
  const router = useRouter();
  const [ filter, setFilter ] = useState('0')
  const [rating, setRating ] = useState(false)
  const [stock, setStock] = useState(false)
  useEffect(() => {
    axios.get('https://recruitment.dev.rollingglory.com/api/v2/gifts?page[number]=1&page[size]=20')
    .then((response) => {setPost(response.data.data);
    });
  }, []);

  useEffect(() => {
    if ( filter === '1') {
      const newData = post?.sort(((a ,b) => (a.attributes.isNew, b.attributes.isNew) ?  1 : -1))
      setPost(newData)
    } else if (filter === '2') {
      const newData = post?.sort(((a ,b) => (a.attributes.rating, b.attributes.rating) ?  1 : -1))
      setPost(newData)
    }
    if (rating) {
      const newData = post.filter((row) => row.attributes.rating > 4)
      setPost(newData)
    }
    if (stock) {
      const newData = post.filter((row) => row.attributes.stock > 1)
      setPost(newData)
    }
  }, [filter, rating, stock])
  
  const handleValue = (e) => {
    setFilter(e)

  }
  function handleChecked (e, a) {
    if (a === 'rating') {
  
      setRating(e)
    } else if (a === 'stock') {
      setStock(e)
    }
  }
  const handleViewDetail = (a) => {
      router.push(`/detail?itemId=${a}`)
  }
  return (
    <div className='container'>
    <Header />
    <div className="row">
      <div className="col">
        <div className='border-bottom'>

          <h5 className='mt-2 mb-4'>Filter</h5>
        </div>
        <CardSmall>
          <div className='fs-6 d-flex justify-content-between mt-3 mx-2'><p>Rating 4 keatas </p>
          <input type='checkbox'  onChange={(e) => handleChecked(e.target.checked, 'rating')} />
          </div>
          <div className='fs-6 d-flex justify-content-between mx-2'><p>Stock tersedia</p>
          <input type='checkbox'  onChange={(e) => handleChecked(e.target.checked, 'stock')} />
          </div>
        </CardSmall>
      </div>
      <div className="col-9 mt-2">
      <div className='border-bottom d-flex justify-content-between'>
      <h5>Product List</h5>
      <div className='d-flex'>
            <h5 className='me-2 mt-1'>Urutkan</h5>
            <select onChange={(e) => handleValue(e.target.value)} className="form-select rounded-pill mb-2 " style={{  width: '150px' }} aria-label="Default select example">
            <option value='0'>All</option>
              <option value="1">Terbaru</option>
              <option value="2">Ulasan</option>
            </select>
          </div>
      </div>
      <div className='row'>
      {post?.map((data, key) => (
       <div className='col-sm-3 ms-5' key={key} onClick={data.attributes.stock === 0 ? null : () => handleViewDetail(data.id)}>
          <CardSmall background={data.attributes.stock === 0 ? '#EEEEEE' : ''}>
            <div className='mx-3 my-3 d-flex justify-content-center cursor-pointer' style={{  cursor: 'pointer' }}>
                <center>
                <div>
                  <div className='d-flex justify-content-between'>
                  <StatusStok stok={data.attributes.stock} />
                  <div></div>
                  </div>
                  <img src={data.attributes.images[0]} width='150px' height='200px' />
                  <p className='fs-6' style={{  fontFamily: "Times New Roman" }}>{data.attributes.name}</p>
                  <div className='row'>
                  <div className='fs-6 text-success d-flex col' style={{  fontFamily: "Times New Roman" }}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x-circle-fill" viewBox="0 0 16 16">
                      <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z"/>
                  </svg>
                      <div className='col-10 d-flex'>
                          <p className='ms-2'>{data.attributes.points} poins</p>
                          <Rating key={data} total={data.attributes.rating || 1} view={data.attributes.numOfReviews}/>
                      </div>
                  </div>
                      <div className='col-3'>
                          <Like like={data.attributes.isWishlist}  />
                      </div>
                  </div>
                </div>
                </center>
            </div>
        </CardSmall>
       </div>
      ))}
      </div>
      </div>
    </div>
  </div>
  )
}
