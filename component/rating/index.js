/* eslint-disable default-case */
import React, { useEffect, useState } from 'react';

function ColorStart () {
 return (
    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" className="bi bi-star-fill text-warning mt-1" viewBox="0 0 16 16">
    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
   </svg>
 )
}

function NoColorStart () {
    return (
       <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" className="bi bi-star-fill text-secondary mt-1" viewBox="0 0 16 16">
       <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
      </svg>
    )
   }
export default function Rating (props) {
    const {total, view} = props;
    const [rating, setRating] = useState(1)
    useEffect(() => {
        setRating(Math.round(total))
    }, [total])

    switch (rating) {
        case 1 : {
            return (
                <div key={rating} className='d-flex'>
                    <ColorStart /><NoColorStart /><NoColorStart /><NoColorStart /><NoColorStart />&nbsp;<p>{view || 0} views</p>
                </div>
            )
        }
        case 2 : {
            return (
                <div key={rating} className='d-flex'>
                    <ColorStart /><ColorStart /><NoColorStart /><NoColorStart /><NoColorStart />&nbsp;<p>{view || 0} views</p>
                </div>
            )
        }
        case 3 : {
            return (
                <div key={rating} className='d-flex'>
                    <ColorStart /><ColorStart /><ColorStart /><NoColorStart /><NoColorStart />&nbsp;<p>{view || 0} views</p>
                </div>
            )
        }
        case 4 : {
            return (
                <div key={rating} className='d-flex'>
                    <ColorStart /><ColorStart /><ColorStart /><ColorStart /><NoColorStart />&nbsp;<p>{view || 0} views</p>
                </div>
            )
        }
        case 5 : {
            return (
                <div key={rating} className='d-flex'>
                    <ColorStart /><ColorStart /><ColorStart /><ColorStart /><ColorStart />&nbsp;<p>{view || 0} views</p>
                </div>
            )
        }
        default:
                  return (
                    <div key={0} className='d-flex'>
                        <NoColorStart /><NoColorStart /><NoColorStart /><NoColorStart /><NoColorStart />&nbsp;<p>{view || 0} view</p>
                        </div>
                  )
    }
}