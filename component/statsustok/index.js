import React from 'react';

export default function StatusStok (props) {
    const {stok} = props;
    switch (stok) {
        case stok < 5 : {
            return (
                <p>{ 'Stok < 5' }</p>
            )
        }
        case 0 : {
            return (
                <p className='text-danger'>Sold Out</p>
            )
        }
        default:
                  return (
                    <p style={{ color: '#556B2F' }}>In Stok </p>
                  )
    }

}