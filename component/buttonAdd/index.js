import { useState } from "react"

export default function ButtonAdd () {
    const [input, setInput] = useState(0)

    const handlePluss = () => {
        setInput(input + 1)
    }

    const handleMin = () => {
        if (input > 0) {
            setInput(input - 1)
        }
    }
    return (
        <div className=" d-flex justify-content-left">
            <button type="button" onClick={() => handleMin()} className="btn btn-outline-primary" style={{ width: '70px', height: '40px' }}>-</button>
            <div style={{ width: '70px', height: '40px' }} className='text-center mt-2'>{input}</div>
            <button type="button" onClick={() => handlePluss()} className="btn btn-outline-primary" style={{ width: '70px', height: '40px' }}>+</button>
        </div>
    )
}