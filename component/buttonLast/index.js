import Like from "../like";

export default function ButtonLast (props) {
    const { like } = props;
    return (
        <div className="d-flex mt-3">
            <Like like={like}  />
            <button type="button" className="btn btn-success rounded-pill ms-3 pt-1" style={{  width: '120px', height: '32px' }}>Redeem</button>
            <button type="button" className="btn btn-outline-success rounded-pill ms-3 pt-1" style={{  width: '120px', height: '32px' }}>Add to cart</button>
        </div>
    )
}