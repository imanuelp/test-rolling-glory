export default function CardSmall (props) {
    const { children, background } = props;
    return (
        <div className="border mt-1" style={{  background: background || null, width: '100%' }}>{children}</div>
    )
}