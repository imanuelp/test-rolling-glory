export const DataBase = {
    "meta": {
        "totalItems": 20,
        "currentPage": 1,
        "itemPerPage": 6,
        "totalPages": 4
    },
    "data": [
        {
            "id": "1",
            "type": "gift",
            "attributes": {
                "id": 1,
                "name": "Xiaomi Mi A2 Lite",
                "info": "<p>The Mi A2 Lite is not your regular budget smartphone - it has a high-res notched screen with slim bezels, a dual-camera on its back, a very large battery, and runs on the latest vanilla Android OS. The notched screen is probably the highlight feature of the Mi A2 Lite and while controversial, it still adds for some modern looks.</p>",
                "description": "<p>\nBody: Aluminum body, plastic antenna strips at the back.<br />\nDisplay: 5.84\" IPS LCD with a cutout, 1,080x2,280px resolution, 19:9 aspect ratio, 432ppi.<br />\nRear camera: Primary 12MP, 1.25µm pixel size, f/2.2 aperture, PDAF; Secondary 5MP, 1.12µm pixel size, f/2.2 aperture - serving as depth sensor; 1080p@30fps video with EIS.<br />\nFront camera: 5MP, Auto HDR; 1080p/30fps video recording.<br />\nOS: Android 8.1 Oreo (Android One)<br />\nChipset: Qualcomm Snapdragon 625: octa-core 2.0GHz Cortex-A53 CPU, Adreno 506 GPU.<br />\nMemory: 3/3GB of RAM; 32/64 storage; microSD slot.<br />\nBattery: 4,000mAh Li-Po (sealed);<br />\nConnectivity: Dual-SIM; LTE, microUSB; Wi-Fi a/b/g/n; GPS; Bluetooth 4.2, IR blaster.<br />\nMisc: Rear-mounted fingerprint reader; single down-firing speaker; 3.5mm jack.\n</p>",
                "points": 500000,
                "slug": "xiaomi-mi-a2-lite",
                "stock": 49,
                "images": [
                    "https://rgbtest.s3.ap-southeast-1.amazonaws.com/images/gift/full/xiaomi-mi-a2-lite.jpg"
                ],
                "isNew": 0,
                "rating": 4.05,
                "numOfReviews": 17,
                "isWishlist": 0
            }
        },
        {
            "id": "4",
            "type": "gift",
            "attributes": {
                "id": 4,
                "name": "Samsung Galaxy S9",
                "info": "<p>It's true what they say - you don't fix what's not broken. And that's what Samsung did for the Galaxy S9 - it didn't change what was already great, it just tweaked specs wherever possible. And it has worked out just fine for them.</p>",
                "description": "<p> \nBody: Polished aluminum frame, Gorilla Glass 5 front and back; IP68 certified for water and dust resistance.<br />\nMidnight Black, Coral Blue, Titanium Gray, Lilac Purple color schemes. Display: 5.8\" Super AMOLED Infinity, 2,960x1440px<br />\nresolution, 18.5:9 (2.06:1) aspect ratio, 570ppi, HDR 10 compliant. Rear camera: 12MP, f/1.5 and f/2.4 aperture, dual<br />\npixel phase detection autofocus, OIS; multi-shot image stacking; multi-frame noise reduction; 2160p/60fps video<br />\nrecording; 1080p/240fps; 720p/960fps super slow-mo with automatic motion detection. Front camera: 8MP, f/1.7 aperture,<br />\nautofocus; 1440p/30fps video recording. OS/Software: Android 8.0 Oreo; Samsung UX v.9; Bixby virtual assistant with<br />\nBixby Vision; KNOX with Intelligent Scan Chipset (review unit): Exynos 9810: octa-core CPU (4x2.7 GHz 3rd-gen Mongoose +<br />\n4x1.8GHz Cortex-A55), Mali-G72 MP18 GPU. Chipset (US and China): Qualcomm Snapdragon 845: octa-core CPU (4x2.8 GHz Kryo<br />\n385 Gold & 4x1.7 GHz Kryo 385 Silver), Adreno 630 GPU. Memory: 4GB of RAM; 64GB / 128GB / 256GB storage; microSD slot<br />\nfor cards up to 256GB, UFS cards support. Battery: 3,000mAh; Adaptive Fast Charging; WPC&PMA wireless charging.<br />\nConnectivity: Single-SIM, Dual-SIM available in certain markets; LTE-A, 4-Band carrier aggregation, Cat.18 downlink; USB<br />\nType-C (v3.1); Wi-Fi a/b/g/n/ac; GPS, Beidou, Galileo; NFC; Bluetooth 5.0. Misc: Fingerprint reader; iris<br />\nrecognition/face recognition; Stereo Speakers with Dolby Atmos; 3.5mm jack; bundled AKG headphones. \n</p>",
                "points": 15000,
                "slug": "samsung-galaxy-s9",
                "stock": 0,
                "images": [
                    "https://rgbtest.s3.ap-southeast-1.amazonaws.com/images/gift/full/samsung-galaxy-s9.jpg"
                ],
                "isNew": 1,
                "rating": 4.79,
                "numOfReviews": 147,
                "isWishlist": 0
            }
        },
        {
            "id": "6",
            "type": "gift",
            "attributes": {
                "id": 6,
                "name": "Oppo F7",
                "info": "<p>Selfies are an inevitable part of our lives and they have prevailed over the skeptics into a bright, if not predominant future. Oppo was among the first makers to helm the niche with the Selfie Expert F series. And now the company is the first ever to offer a stunning 25MP selfie camera with the new Oppo F7.</p>",
                "description": "<p> \nBody: Plastic back and frame, Corning Gorilla Glass 5 front<br />\nDisplay: 6.23-inch IPS LCD, 1080x2280px, 405ppi<br />\nOS: Android 8.1 Oreo with ColorOS 5.0 on top<br />\nChipset: Mediatek Helio P60, 4x2.0 GHz Cortex-A73 & 4x2.0 GHz Cortex-A53, octa-core CPU, Mali-G72 MP3 GPU, 4 or 6 GB RAM<br />\nStorage: 64 or 128 GB internal with microSD hybrid (SIM2) expansion slot<br />\nRear Camera: 16MP, f/1.8, PDAF, LED flash<br />\nVideo recording: 1080p@30 fps;<br />\nFront Camera: 25MP, f/2.0, 1080p video recording<br />\nConnectivity: Dual SIM, Dual 4G VoLTE, Bluetooth 4.2, dual-band Wi-Fi 802.11ac, A-GPS/GLONASS, FM radio, microUSB 2.0<br />\nBattery: 3,400mAh non-removable, Regular Charge 5V/2A<br />\nMisc: Fingerprint reader (rear-mounted)\n</p>",
                "points": 42000,
                "slug": "oppo-f7",
                "stock": 24,
                "images": [
                    "https://rgbtest.s3.ap-southeast-1.amazonaws.com/images/gift/full/oppo-f7.jpg"
                ],
                "isNew": 0,
                "rating": 3.86,
                "numOfReviews": 223,
                "isWishlist": 0
            }
        },
        {
            "id": "15",
            "type": "gift",
            "attributes": {
                "id": 15,
                "name": "Samsung Galaxy S8",
                "info": "<p>When 5 inches is the de-facto standard for a compact Android handset, yet you want to prove that more is always better, what do you do? Well, it depends on the definition of more. You can do something crazy, stupid or impossible. Samsung? They decided to be sensible. Design a phone, build a screen, put the screen where it always goes. Business as usual.</p>",
                "description": "<p> \nBody: Polished aluminum frame, Gorilla Glass 5 front and rear; IP68 certified for water and dust resistance. Arctic Silver, Orchid Grey, Black Sky, Maple Gold, and Coral Blue color schemes.<br />\nDisplay: 5.8\" Super AMOLED, 2,960x1440px resolution, 18.5:9 (2.06:1) aspect ratio, 570ppi; HDR 10 compliant (no Dolby Vision).<br />\nRear camera:12MP, f/1.7 aperture, dual pixel phase detection autofocus, OIS; multi-shot image stacking; 2160p/30fps video recording.<br />\nFront camera: 8MP, f/1.7 aperture, autofocus; 1440p/30fps video recording.<br />\nOS/Software: Android 7.0 Nougat; Bixby virtual assistant.<br />\nChipsets: Qualcomm Snapdragon 835: octa-core CPU (4xKryo 280 + 4xCortex-A53), Adreno 540 GPU. Exynos 8895: octa-core CPU (4x2nd-gen Mongoose + 4xCortex-A53), Mali-G71 GPU.<br />\nMemory: 4GB of RAM (a 6GB option likely in some markets, later down the line); 64GB of storage; microSD slot up to 256GB, UFS cards support.<br />\nBattery: 3,000mAh Li-Ion (sealed); Adaptive Fast Charging (same as S7); QuickCharge 2.0 support; WPC&PMA wireless charging.<br />\nConnectivity: Single-SIM, Dual-SIM available in certain markets; LTE-A, 4-Band carrier aggregation, Cat.16/13 (1Gbps/150Mbps); USB Type-C (v3.1); Wi-Fi a/b/g/n/ac; GPS, Beidou, Galileo; NFC; Bluetooth 5.0.<br />\nMisc: Fingerprint reader; iris recognition/face recognition; single speaker on the bottom; 3.5mm jack; bundled AKG headphones.<br />\n</p>",
                "points": 6500,
                "slug": "samsung-galaxy-s8",
                "stock": 0,
                "images": [
                    "https://rgbtest.s3.ap-southeast-1.amazonaws.com/images/gift/full/samsung-galaxy-s8.jpg"
                ],
                "isNew": 1,
                "rating": 3.15,
                "numOfReviews": 895,
                "isWishlist": 0
            }
        },
        {
            "id": "18",
            "type": "gift",
            "attributes": {
                "id": 18,
                "name": "Xiaomi Pocophone F1",
                "info": "<p>Xiaomi's announcements have been relatively predictable so far, but the introduction of a new Poco sub-brand was a surprising move. It sounds like a marketing experiment - deliver flagship specs at cutthroat prices. Sound familiar? Well, it does remind of how OnePlus came to be, but we're not sure the new Pocophone F1 is headed in the same direction.</p>",
                "description": "<p> \nBody: Plastic frame, Kevlar or Plastic back, Gorilla Glass front<br />\nDisplay: 6.18\" IPS LCD, 2,246x1,080px resolution, 18.7:9 aspect ratio, 403ppi.<br />\nRear camera: Primary 12MP, Type 1/2.55\" sensor, 1.4µm pixel size, f/1.9 aperture, dual pixel PDAF; Secondary 5MP - serving as depth sensor. 2160p/30fps, 1080p/240fps slow motion.<br />\nFront camera: 20MP, 0.9µm pixel size, f/2.0 aperture; 1080p/30fps video recording.<br />\nOS: Android 8.1 Oreo; MIUI 9.5 for Poco custom overlay, MIUI 10 on the way.<br />\nChipset: Qualcomm Snapdragon 845: octa-core CPU (4x2.8 GHz Kryo 385 Gold & 4x1.7 GHz Kryo 385 Silver), Adreno 630 GPU.<br />\nMemory: 6/8GB of RAM; 64/128/256GB storage; (hybrid) microSD slot.<br />\nBattery: 4,000mAh Li-Po; QuickCharge 3.0 fast charging.<br />\nConnectivity: Dual-SIM; LTE-A, 4-Band carrier aggregation, Cat.16/13 (1Gbps/150Mbps); USB-C; Wi-Fi a/b/g/n/ac; dual-band GPS; Bluetooth 5.0; FM radio<br />\nMisc: Rear-mounted fingerprint reader; stereo speakers; infrared face recognition; 3.5mm jack.\n</p>",
                "points": 13000,
                "slug": "xiaomi-pocophone-f1",
                "stock": 9,
                "images": [
                    "https://rgbtest.s3.ap-southeast-1.amazonaws.com/images/gift/full/xiaomi-pocophone-f1.jpg"
                ],
                "isNew": 0,
                "rating": 3.26,
                "numOfReviews": 824,
                "isWishlist": 0
            }
        },
        {
            "id": "19",
            "type": "gift",
            "attributes": {
                "id": 19,
                "name": "Apple iPhone X 64GB",
                "info": "<p>The anniversary iPhone is here. The iPhone X. Or the iPhone 10. The name is as confusing as the product itself is game-changing. Tim Cook's vision finally overwhelmed Steve Jobs ideological remnants. A new dawn for the iPhones has begun, free of the iconic Home key and the notorious screen bezels.</p>",
                "description": "<p> \nBody: Stainless steel frame, reinforced glass front, and rear, IP67 certified for water and dust resistance. Space Gray, and Silver color options.<br />\nScreen: 5.8\" bezel-less Super AMOLED screen of 1125 x 2436px resolution, 458ppi. HDR video support, wide color gamut. True Tone adjustment via a six-channel ambient light sensor, 3D Touch.<br />\nOS: Apple iOS 11<br />\nChipset: Hexa-core (2 Monsoon + 4 Mistral) 2.39GHz Apple CPU, tri-core Apple GPU, Apple A11 Bionic SoC<br />\nMemory: 3GB of RAM; 64/256GB of internal storage (non-expandable)<br />\nCamera: Dual 12MP camera: wide-angle F/1.8 with OIS + telephoto F/2.4 with OIS, live bokeh effects (including Portrait mode and Portrait Lightning), 2x lossless zoom, quad-LED flash with slow sync, phase detection auto focus, wide color gamut capture<br />\nVideo recording: 2160p@60/30fps, 1080p@30/60/120/240fps video recording<br />\nSelfie: 7MP F/2.2 front-facing camera with BSI sensor and HDR mode, 1080p@30fps video, depth detection for Portrait mode and animoji<br />\nStorage: 64GB or 256GB of built-in storage<br />\nConnectivity: 4G LTE Cat.12 (600Mbps); Wi-Fi a/b/g/n/ac; Bluetooth 5.0; Lightning port; GPS with A-GPS, GLONASS, GALILEO, QZSS; NFC (Apple, NFC tag reading)<br />\nBattery: 2,716mAh battery, wireless charging (Qi compatible)<br />\nMisc: Face ID through dedicated TrueDepth camera, Stereo speakers, Taptic Engine<br />\n</p>",
                "points": 2000,
                "slug": "apple-iphone-x-64gb",
                "stock": 19,
                "images": [
                    "https://rgbtest.s3.ap-southeast-1.amazonaws.com/images/gift/full/apple-iphone-x.jpg"
                ],
                "isNew": 0,
                "rating": 4.61,
                "numOfReviews": 25,
                "isWishlist": 0
            }
        }
    ],
    "links": {
        "self": "https://recruitment.dev.rollingglory.com/api/v2/gifts?page[number]=1&page[size]=6",
        "next": "https://recruitment.dev.rollingglory.com/api/v2/gifts?page[number]=2&page[size]=6",
        "last": "https://recruitment.dev.rollingglory.com/api/v2/gifts?page[number]=4&page[size]=6"
    }
}